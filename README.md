# Bract
This is a cross-platform fractal browising app based on the bevy game engine and a custom library to render fractals.

## The Name
The name `bract` is a botany term for a part of a flower. From the Oxford Language Dictionary:
> A bract is a modified leaf or scale, typically small, with a flower or flower cluster in its axil. 

## Running Bract
- Have a working install of the rust Nightly toolchain.
- `cargo run`

## Building a release binary
- `cargo build --profile release`

## Keys:
- middle-click to center the view at the cursor
- left-click to zoom in
- right-click to zoom out
- press `a` to cycle between fractal algorithms
- press `c` to cycle between coloring algorithms
- press `Home` to reset the view, alg, etc
- press `,` to decrease the exponent used 
- press `.` to increase the exponent used
- press `d` to dump the JSON that describes the current state

## Screenshots
A basic Mandelbrot with smooth coloring:
![mandelbrot-smooth.png](images/mandelbrot-smooth.png)
A zoomed-in Mandelbrot with zebra-stripe coloring, every 10th band is blue: 
![mandelbrot-zebra-mod.png](images/mandelbrot-zebra-mod.png)
A Julia fractal with an exponent of 7 and zebra-stripe coloring:
![julia-e7-zebra.png](images/julia-e7-zebra.png)
A burning ship fractal with basic banded coloring:
![burningship-bands.png](images/burningship-bands.png)
A Mandelbrot fractal with an exponent of -6 and basic banded coloring:
![mandelbrot-n6-bands.png](images/mandelbrot-n6-bands.png)
A Tricorn fractal with basic baned coloring:
![tricorn-bands.png](images/tricorn-bands.png)

## TODO:
- Supersampling (in progress)
- Issue: Click-to-focus also zooms in
- The Algorithm and ColorMap cycling are hackish. 
- Find a better way to encapsulate ColorMap and Algorithm. There are dependencies between them, like ColorMap::Smooth requiring a large escape distance.
- Arbitrary presicision
- Documentation Comments
- Doc Tests
- More Tests
- UI for parameter editing?
- Optimizations
- Profile performance
- Look for resource leaks around image rendering
