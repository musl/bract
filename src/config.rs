use bevy::prelude::{Color, Resource};
use bevy::window::PresentMode;

use bract_lib::prelude::*;
use serde::{Deserialize, Serialize};

// TODO: split this up into application state and
#[derive(Clone, Debug, Resource, Deserialize, Serialize)]
pub struct Config {
    pub clear_color: Color,
    pub frame_rate: f64,
    pub height: f32,
    pub home_view: View,
    pub job: Job,
    pub log_filter: &'static str,
    pub resizable: bool,
    pub title: &'static str,
    pub vsync: bool,
    pub width: f32,
}

impl Default for Config {
    fn default() -> Self {
        let (width, height) = (1280.0, 720.0);
        let home_view = View::new(width as u32, height as u32, -2.6667, -1.5, 2.6667, 1.5);

        Self {
            clear_color: Color::BLACK,
            frame_rate: 24.0,
            height: height,
            home_view: home_view,
            job: Job {
                algorithm: Algorithm::default(),
                view: home_view,
                sample_count: 1,
            },
            log_filter: "naga=error,wgpu_core=error,wgpu_hal=error",
            resizable: false,
            title: "bract",
            vsync: false,
            width: width,
        }
    }
}

impl Config {
    pub fn present_mode(&self) -> PresentMode {
        match self.vsync {
            true => PresentMode::AutoVsync,
            false => PresentMode::AutoNoVsync,
        }
    }
}
