use bevy::core_pipeline::clear_color::ClearColorConfig;
use bevy::log::{Level, LogPlugin};
use bevy::prelude::*;
use bevy::window::PrimaryWindow;
use clap::Parser;
use image::DynamicImage;
use std::time::Instant;

use bract_lib::prelude::*;

mod config;
use config::Config;

#[derive(Resource, Clone, Deref)]
struct FractalImage(Handle<Image>);

#[derive(Clone, Copy, Debug, Parser)]
#[command(about, author, long_about = None, version)]
pub struct Cli {
    #[arg(short, long, default_value_t = Level::INFO)]
    pub log_level: Level,
}

fn main() {
    let cli = Cli::parse();
    let config = Config::default();

    let window = Window {
        title: config.title.into(),
        resizable: config.resizable,
        resolution: (config.width, config.height).into(),
        present_mode: config.present_mode(),
        ..default()
    };

    let plugins = DefaultPlugins
        .set(AssetPlugin {
            watch_for_changes: true,
            ..default()
        })
        .set(LogPlugin {
            level: cli.log_level,
            filter: config.log_filter.into(),
        })
        .set(WindowPlugin {
            primary_window: Some(window),
            ..default()
        });

    App::new()
        .add_plugins(plugins)
        .insert_resource(config)
        .add_startup_system(setup)
        .add_system(input)
        .run();
}

fn render(job: &Job) -> Image {
    let start = Instant::now();
    let image = Image::from_dynamic(DynamicImage::ImageRgb8(job.render()), false);
    debug!("render: {:0.6} seconds", start.elapsed().as_secs_f64());

    image
}

#[derive(Component)]
struct DebugText;

fn setup(mut commands: Commands, mut images: ResMut<Assets<Image>>, config: Res<Config>) {
    debug!(
        "Config: {}",
        serde_json::to_string_pretty(&(*config)).unwrap()
    );

    commands.spawn(Camera2dBundle {
        camera_2d: Camera2d {
            clear_color: ClearColorConfig::Custom(config.clear_color),
            ..default()
        },
        ..default()
    });

    let handle = images.add(render(&config.job));
    commands.insert_resource(FractalImage(handle.clone()));
    commands.spawn(SpriteBundle {
        sprite: Sprite {
            custom_size: Some(Vec2 {
                x: config.width,
                y: config.height,
            }),
            ..default()
        },
        texture: handle,
        ..default()
    });
}

fn input(
    mut config: ResMut<Config>,
    mut images: ResMut<Assets<Image>>,
    handle: ResMut<FractalImage>,
    buttons: Res<Input<MouseButton>>,
    q_windows: Query<&Window, With<PrimaryWindow>>,
    keys: Res<Input<KeyCode>>,
) {
    if let Some(position) = q_windows.single().cursor_position() {
        if buttons.just_released(MouseButton::Left) {
            config.job.view.scale(0.5);
            let _ = images.set(handle.0.clone(), render(&config.job));
            return;
        }

        if buttons.just_released(MouseButton::Right) {
            config.job.view.scale(2.0);
            let _ = images.set(handle.0.clone(), render(&config.job));
            return;
        }

        if buttons.just_released(MouseButton::Middle) {
            let (x, y) = (position.x as u32, (config.height - position.y) as u32);
            config.job.view.center(x, y);
            let _ = images.set(handle.0.clone(), render(&config.job));
            return;
        }

        if keys.just_released(KeyCode::Home) {
            config.job.view = config.home_view;
            config.job.algorithm = Algorithm::default();
            let _ = images.set(handle.0.clone(), render(&config.job));
            return;
        }

        if keys.just_released(KeyCode::Period) {
            config.job.algorithm.increment_power();
            let _ = images.set(handle.0.clone(), render(&config.job));
            return;
        }

        if keys.just_released(KeyCode::Comma) {
            config.job.algorithm.decrement_power();
            let _ = images.set(handle.0.clone(), render(&config.job));
            return;
        }

        if keys.just_released(KeyCode::A) {
            config.job.algorithm = config.job.algorithm.next().unwrap();
            let _ = images.set(handle.0.clone(), render(&config.job));
            return;
        }

        if keys.just_released(KeyCode::C) {
            config.job.algorithm.next_color_map();
            let _ = images.set(handle.0.clone(), render(&config.job));
            return;
        }

        if keys.just_released(KeyCode::D) {
            info!("\n{}", serde_json::to_string_pretty(&config.job).unwrap());
            return;
        }
    }
}
