use anyhow::Result;

use bract_lib::prelude::*;

pub fn main() -> Result<()> {
    /*
    let job_json = r#"{
        "algorithm": {
            "Mandelbrot": {
                "d_max":4.0,
                "i_max":1000,
                "power":2
            }
        },
        "color_map": {
            "Bands": {
                "in_color":[0,0,0]
            }
        },
        "view": {
            "image": {
                "width":5000,
                "height":5000
            },
            "plane": {
                "min": [-3.0,-3.0],
                "max": [3.0,3.0]
            }
        }
    }"#;

    let j: Job = serde_json::from_str(&job_json).unwrap();
    */

    let j = Job::default();

    j.render().save("test.png")?;

    Ok(())
}
