use image::Rgb;
use serde::{
    ser::SerializeSeq, 
    de::{
        self,
        SeqAccess,
        Visitor,
    },
    Serializer,
    Deserializer,
};
use std::fmt;

pub fn deserialize<'de, D>(deserializer: D) -> Result<Rgb<u8>, D::Error>
where
    D: Deserializer<'de>,
{
    struct RgbU8Visitor;

    impl<'de> Visitor<'de> for RgbU8Visitor {
        type Value = Rgb<u8>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("Rgb<u8>")
        }

        fn visit_seq<V>(self, mut seq: V) -> Result<Rgb<u8>, V::Error>
        where
            V: SeqAccess<'de>,
        {
            let r: u8 = seq.next_element()?
                .ok_or_else(|| de::Error::invalid_length(0, &self))?;
            let g: u8 = seq.next_element()?
                .ok_or_else(|| de::Error::invalid_length(1, &self))?;
            let b: u8 = seq.next_element()?
                .ok_or_else(|| de::Error::invalid_length(2, &self))?;
            Ok(Rgb([r, g, b]))
        }
    }

    deserializer.deserialize_seq(RgbU8Visitor)
}

pub fn serialize<S>(rgb: &Rgb<u8>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let mut seq = serializer.serialize_seq(Some(3))?;
    for v in rgb.0 {
        seq.serialize_element(&v)?;
    }
    seq.end()
}
