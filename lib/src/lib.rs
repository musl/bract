pub mod algorithm;
pub mod color_map;
pub mod job;
pub mod prelude;
pub mod view;

mod serde_rgbu8;
mod util;
