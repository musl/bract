use image::Rgb;
use num_complex::Complex;
use serde::{Deserialize, Serialize};
use strum::EnumIter;

use crate::prelude::{Colorize, EscapeColorMap, IndexColorMap};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct EscapeResult {
    pub i: Option<u64>,
    pub i_max: u64,
    pub z: Complex<f64>,
    pub d_max: f64,
    pub power: i32,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct SearchResult {
    pub i: Option<u64>,
    pub i_max: u64,
}

pub trait Execute: Sync + Send {
    fn execute(&self, z0: Complex<f64>) -> Rgb<u8>;
}

#[derive(Clone, Copy, Debug, EnumIter, PartialEq, Serialize, Deserialize)]
pub enum Algorithm {
    BurningShip(BurningShip),
    Julia(Julia),
    Mandelbrot(Mandelbrot),
    Newton(Newton),
    Nova(Nova),
    Tricorn(Tricorn),
}

impl Default for Algorithm {
    fn default() -> Self {
        Self::Mandelbrot(Mandelbrot::default())
        //Self::Nova(Nova::default())
    }
}

impl Execute for Algorithm {
    fn execute(&self, z0: Complex<f64>) -> Rgb<u8> {
        match self {
            Self::BurningShip(a) => a.execute(z0),
            Self::Julia(a) => a.execute(z0),
            Self::Mandelbrot(a) => a.execute(z0),
            Self::Newton(a) => a.execute(z0),
            Self::Nova(a) => a.execute(z0),
            Self::Tricorn(a) => a.execute(z0),
        }
    }
}

impl Iterator for Algorithm {
    type Item = Self;

    fn next(&mut self) -> Option<Self::Item> {
        Some(match self {
            Self::Mandelbrot(_) => Self::Julia(Julia::default()),
            Self::Julia(_) => Self::Tricorn(Tricorn::default()),
            Self::Tricorn(_) => Self::BurningShip(BurningShip::default()),
            Self::BurningShip(_) => Self::Newton(Newton::default()),
            Self::Newton(_) => Self::Nova(Nova::default()),
            Self::Nova(_) => Self::Mandelbrot(Mandelbrot::default()),
        })
    }
}

impl Algorithm {
    pub fn next_color_map(&mut self) {
        match self {
            Self::Mandelbrot(a) => {
                a.color_map = a.color_map.next().unwrap();
                match a.color_map {
                    EscapeColorMap::Smooth(_) => a.d_max = 1e6,
                    _ => a.d_max = 4.0,
                }
            }
            Self::Julia(a) => {
                a.color_map = a.color_map.next().unwrap();
                match a.color_map {
                    EscapeColorMap::Smooth(_) => a.d_max = 1e6,
                    _ => a.d_max = 4.0,
                }
            }
            Self::Tricorn(a) => {
                a.color_map = a.color_map.next().unwrap();
                match a.color_map {
                    EscapeColorMap::Smooth(_) => a.d_max = 1e6,
                    _ => a.d_max = 4.0,
                }
            }
            Self::BurningShip(a) => {
                a.color_map = a.color_map.next().unwrap();
                match a.color_map {
                    EscapeColorMap::Smooth(_) => a.d_max = 1e6,
                    _ => a.d_max = 4.0,
                }
            }
            Self::Nova(a) => {
                a.color_map = a.color_map.next().unwrap();
                match a.color_map {
                    EscapeColorMap::Smooth(_) => a.d_max = 10e6,
                    _ => a.d_max = 4.0,
                }
            }
            Self::Newton(a) => a.color_map = a.color_map.next().unwrap(),
        }
    }

    pub fn increment_power(&mut self) {
        match self {
            Self::Mandelbrot(a) => a.power = (a.power + 1) % 100,
            Self::Julia(a) => a.power = (a.power + 1) % 100,
            Self::Tricorn(a) => a.power = (a.power + 1) % 100,
            Self::BurningShip(a) => a.power = (a.power + 1) % 100,
            Self::Nova(a) => a.power = (a.power + 1) % 100,
            _ => {}
        }
    }

    pub fn decrement_power(&mut self) {
        match self {
            Self::Mandelbrot(a) => a.power = (a.power - 1) % 100,
            Self::Julia(a) => a.power = (a.power - 1) % 100,
            Self::Tricorn(a) => a.power = (a.power - 1) % 100,
            Self::BurningShip(a) => a.power = (a.power - 1) % 100,
            Self::Nova(a) => a.power = (a.power - 1) % 100,
            _ => {}
        }
    }
}

pub struct PeriodicityChecker {
    pub max_p: i32,

    p: i32,
    z: Complex<f64>,
}

impl Default for PeriodicityChecker {
    fn default() -> Self {
        Self {
            max_p: 20,
            p: 0,
            z: Complex::new(0.0, 0.0),
        }
    }
}

impl PeriodicityChecker {
    pub fn new(max_p: i32) -> Self {
        Self {
            max_p,
            p: 0,
            z: Complex::new(0.0, 0.0),
        }
    }

    pub fn check(&mut self, z: Complex<f64>) -> bool {
        // Periodicity checking
        if self.p > 20 && z == self.z {
            return true;
        }

        self.p += 1;
        if self.p > 20 {
            self.p = 0;
            self.z = z;
        }

        return false;
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Julia {
    pub color_map: EscapeColorMap,
    pub c: Complex<f64>,
    #[serde(default)]
    pub d_max: f64,
    #[serde(default)]
    pub i_max: u64,
    #[serde(default)]
    pub power: i32,
}

impl Default for Julia {
    fn default() -> Self {
        Self {
            color_map: EscapeColorMap::default(),
            c: Complex::new(-0.4, 0.6),
            d_max: 4.0,
            i_max: 1000,
            power: 2,
        }
    }
}

impl Execute for Julia {
    fn execute(&self, z0: Complex<f64>) -> Rgb<u8> {
        let mut i = 0;
        let mut z = z0.clone();
        let mut pc = PeriodicityChecker::default();

        while i < self.i_max && z.norm() < self.d_max {
            z = z.powi(self.power) + self.c;
            i += 1;

            if pc.check(z) {
                i = self.i_max;
                break;
            }
        }

        if i == self.i_max {
            return self.color_map.colorize(&EscapeResult {
                d_max: self.d_max,
                i: None,
                i_max: self.i_max,
                power: self.power,
                z,
            });
        }

        self.color_map.colorize(&EscapeResult {
            d_max: self.d_max,
            power: self.power,
            i: Some(i),
            i_max: self.i_max,
            z,
        })
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Mandelbrot {
    pub color_map: EscapeColorMap,
    pub d_max: f64,
    pub i_max: u64,
    pub power: i32,
}

impl Default for Mandelbrot {
    fn default() -> Self {
        Self {
            color_map: EscapeColorMap::default(),
            d_max: 4.0,
            i_max: 1000,
            power: 2,
        }
    }
}

impl Execute for Mandelbrot {
    fn execute(&self, z0: Complex<f64>) -> Rgb<u8> {
        let mut i = 0;
        let mut z = z0.clone();
        let mut pc = PeriodicityChecker::default();

        while i < self.i_max && z.norm() < self.d_max {
            z = z.powi(self.power) + z0;
            i += 1;

            if pc.check(z) {
                i = self.i_max;
                break;
            }
        }

        if i == self.i_max {
            return self.color_map.colorize(&EscapeResult {
                d_max: self.d_max,
                i: None,
                i_max: self.i_max,
                power: self.power,
                z,
            });
        }

        self.color_map.colorize(&EscapeResult {
            d_max: self.d_max,
            power: self.power,
            i: Some(i),
            i_max: self.i_max,
            z,
        })
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Tricorn {
    pub color_map: EscapeColorMap,
    pub d_max: f64,
    pub i_max: u64,
    pub power: i32,
}

impl Default for Tricorn {
    fn default() -> Self {
        Self {
            color_map: EscapeColorMap::default(),
            d_max: 4.0,
            i_max: 5000,
            power: 2,
        }
    }
}

impl Execute for Tricorn {
    fn execute(&self, z0: Complex<f64>) -> Rgb<u8> {
        let mut i = 0;
        let mut z = z0.clone();
        let mut pc = PeriodicityChecker::default();

        while i < self.i_max && z.norm() < self.d_max {
            z = z.conj().powi(self.power) + z0;
            i += 1;

            if pc.check(z) {
                i = self.i_max;
                break;
            }
        }

        if i == self.i_max {
            return self.color_map.colorize(&EscapeResult {
                d_max: self.d_max,
                i: None,
                i_max: self.i_max,
                power: self.power,
                z,
            });
        }

        self.color_map.colorize(&EscapeResult {
            d_max: self.d_max,
            power: self.power,
            i: Some(i),
            i_max: self.i_max,
            z,
        })
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct BurningShip {
    pub color_map: EscapeColorMap,
    pub d_max: f64,
    pub i_max: u64,
    pub power: i32,
}

impl Default for BurningShip {
    fn default() -> Self {
        Self {
            color_map: EscapeColorMap::default(),
            d_max: 4.0,
            i_max: 1000,
            power: 2,
        }
    }
}

impl Execute for BurningShip {
    fn execute(&self, z0: Complex<f64>) -> Rgb<u8> {
        let z0 = z0.conj();

        let mut i = 0;
        let mut z = z0.clone();
        let mut pc = PeriodicityChecker::default();

        while i < self.i_max && z.norm() < self.d_max {
            z = Complex::new(z.re.abs(), z.im.abs()).powi(self.power) + z0;
            i += 1;

            if pc.check(z) {
                i = self.i_max;
                break;
            }
        }

        if i == self.i_max {
            return self.color_map.colorize(&EscapeResult {
                d_max: self.d_max,
                i: None,
                i_max: self.i_max,
                power: self.power,
                z,
            });
        }

        self.color_map.colorize(&EscapeResult {
            d_max: self.d_max,
            power: self.power,
            i: Some(i),
            i_max: self.i_max,
            z,
        })
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Nova {
    pub color_map: EscapeColorMap,
    pub d_max: f64,
    pub i_max: u64,
    pub power: i32,
}

impl Default for Nova {
    fn default() -> Self {
        Self {
            color_map: EscapeColorMap::default(),
            d_max: 4.0,
            i_max: 1000,
            power: 2,
        }
    }
}

impl Execute for Nova {
    fn execute(&self, z0: Complex<f64>) -> Rgb<u8> {
        let z0 = z0.conj();

        let mut i = 0;
        let mut z = z0.clone();
        let mut pc = PeriodicityChecker::default();

        while i < self.i_max && z.norm() < self.d_max {
            // z = z - (z.powi(self.power + 1) - 1.0) / 3.0 * z.powi(self.power) + z0;
            z = z - (z.powi(self.power + 1) - 1.0) / (3.0 * z.powi(self.power)) + z0;
            i += 1;

            if pc.check(z) {
                i = self.i_max;
                break;
            }
        }

        if i == self.i_max {
            return self.color_map.colorize(&EscapeResult {
                d_max: self.d_max,
                i: None,
                i_max: self.i_max,
                power: self.power,
                z,
            });
        }

        self.color_map.colorize(&EscapeResult {
            d_max: self.d_max,
            power: self.power,
            i: Some(i),
            i_max: self.i_max,
            z,
        })
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Newton {
    pub color_map: IndexColorMap,
    pub i_max: u64,
    pub epsilon: f64,
}

impl Default for Newton {
    fn default() -> Self {
        Self {
            color_map: IndexColorMap::default(),
            epsilon: 0.000001,
            i_max: 1000,
        }
    }
}

impl Execute for Newton {
    fn execute(&self, z0: Complex<f64>) -> Rgb<u8> {
        let mut i = 0;
        let mut z = z0.clone();

        let roots = vec![
            Complex::new(1.0, 0.0),
            Complex::new(-0.5, 3.0_f64.sqrt() / 2.0),
            Complex::new(-0.5, -1.0 * 3.0_f64.sqrt() / 2.0),
        ];

        let f = |z: Complex<f64>| z.powi(3) - 1.0;
        let f_prime = |z: Complex<f64>| 3.0 * z.powi(2);

        while i < self.i_max {
            z -= f(z) / f_prime(z);
            for (r, root) in roots.iter().enumerate() {
                let diff = z - root;
                if diff.re.abs() < self.epsilon && diff.im.abs() < self.epsilon {
                    return self.color_map.colorize(&SearchResult {
                        i: Some(r as u64),
                        i_max: roots.len() as u64,
                    });
                }
            }

            i += 1;
        }

        self.color_map.colorize(&SearchResult {
            i: None,
            i_max: roots.len() as u64,
        })
    }
}
