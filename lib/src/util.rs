pub trait UnitToU8<T> {
    fn unit_to_u8(self) -> u8;
}

impl<T> UnitToU8<T> for T
where
    T: Into<f64>,
{
    fn unit_to_u8(self) -> u8 {
        (0xff as f64 * (0.5 + 0.5 * self.into())) as u8
    }
}

pub trait UnitToU16<T> {
    fn unit_to_u8(self) -> u16;
}

impl<T> UnitToU16<T> for T
where
    T: Into<f64>,
{
    fn unit_to_u8(self) -> u16 {
        (0xffff as f64 * (0.5 + 0.5 * self.into())) as u16
    }
}

