use image::RgbImage;
use rayon::ThreadPoolBuilder;
use serde::{Deserialize, Serialize};

use crate::algorithm::{Algorithm, Execute};
use crate::view::View;

// TODO: use lazy_static to make a list of default algs + parameters to seed the undo map with.
// IDEA: Undo map: a HashMap<Algorithm, Vec<Job>> that we can cycle between states and have an "undo" button.

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Job {
    pub algorithm: Algorithm,
    pub view: View,
    pub sample_count: u8,
}

impl Job {
    pub fn new(algorithm: Algorithm, view: View, sample_count: u8) -> Self {
        Self {
            algorithm,
            view,
            sample_count,
        }
    }

    pub fn render(&self) -> RgbImage {
        let pool = ThreadPoolBuilder::new()
            .num_threads(num_cpus::get())
            .build()
            .unwrap();

        let mut image = RgbImage::new(self.view.image.width, self.view.image.height);

        pool.scope(|s| {
            for (_, row) in image.enumerate_rows_mut() {
                s.spawn(|_| {
                    for (x, y, p) in row {
                        // TODO: supersampling, color blending
                        *p = self.algorithm.execute(self.view.pixel_to_plane(x, y));
                    }
                });
            }
        });

        image
    }
}
