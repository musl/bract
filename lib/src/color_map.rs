use image::Rgb;
use palette::{Hsl, IntoColor, Srgb};
use serde::{Deserialize, Serialize};
use strum::EnumIter;

use std::f64::consts::*;

use crate::algorithm::{EscapeResult, SearchResult};
use crate::serde_rgbu8;
use crate::util::UnitToU8;

pub trait Colorize<R>: Sync + Send {
    fn colorize(&self, result: &R) -> Rgb<u8>;
}

#[derive(Clone, Copy, Debug, EnumIter, PartialEq, Serialize, Deserialize)]
pub enum EscapeColorMap {
    Bands(Bands),
    Smooth(Smooth),
    Zebra(Zebra),
    ZebraMod(ZebraMod),
}

impl Default for EscapeColorMap {
    fn default() -> Self {
        Self::Bands(Bands::default())
    }
}

impl Iterator for EscapeColorMap {
    type Item = Self;

    fn next(&mut self) -> Option<Self::Item> {
        Some(match self {
            Self::Bands(_) => Self::Smooth(Smooth::default()),
            Self::Smooth(_) => Self::Zebra(Zebra::default()),
            Self::Zebra(_) => Self::ZebraMod(ZebraMod::default()),
            Self::ZebraMod(_) => Self::Bands(Bands::default()),
        })
    }
}

impl Colorize<EscapeResult> for EscapeColorMap {
    fn colorize(&self, result: &EscapeResult) -> Rgb<u8> {
        match self {
            Self::Bands(a) => a.colorize(result),
            Self::Smooth(a) => a.colorize(result),
            Self::Zebra(a) => a.colorize(result),
            Self::ZebraMod(a) => a.colorize(result),
        }
    }
}

#[derive(Clone, Copy, Debug, EnumIter, PartialEq, Serialize, Deserialize)]
pub enum IndexColorMap {
    Auto(Auto),
}

impl Default for IndexColorMap {
    fn default() -> Self {
        Self::Auto(Auto::default())
    }
}

impl Iterator for IndexColorMap {
    type Item = Self;

    fn next(&mut self) -> Option<Self::Item> {
        Some(match self {
            Self::Auto(_) => Self::Auto(Auto::default()),
        })
    }
}

impl Colorize<SearchResult> for IndexColorMap {
    fn colorize(&self, result: &SearchResult) -> Rgb<u8> {
        match self {
            Self::Auto(a) => a.colorize(result),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Bands {
    #[serde(with = "serde_rgbu8")]
    pub in_color: Rgb<u8>,
}

impl Default for Bands {
    fn default() -> Self {
        Self {
            in_color: Rgb([0, 0, 0]),
        }
    }
}

impl Colorize<EscapeResult> for Bands {
    fn colorize(&self, er: &EscapeResult) -> Rgb<u8> {
        match er.i {
            Some(i) => {
                let t = er.i_max as f64 / FRAC_PI_2 * i as f64 / er.i_max as f64;
                Rgb([
                    t.cos().unit_to_u8(),
                    (t + FRAC_PI_4).sin().unit_to_u8(),
                    t.sin().unit_to_u8(),
                ])
            }
            None => self.in_color,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Smooth {
    #[serde(with = "serde_rgbu8")]
    pub in_color: Rgb<u8>,
}

impl Default for Smooth {
    fn default() -> Self {
        Self {
            in_color: Rgb([0, 0, 0]),
        }
    }
}

impl Colorize<EscapeResult> for Smooth {
    fn colorize(&self, er: &EscapeResult) -> Rgb<u8> {
        match er.i {
            Some(i) => {
                let nu = (er.z.norm().log10() / er.d_max.log10()).log(er.power as f64);
                let t = i as f64 + 1.0 - nu;

                Rgb([
                    t.cos().unit_to_u8(),
                    (t + FRAC_PI_4).sin().unit_to_u8(),
                    t.sin().unit_to_u8(),
                ])
            }
            None => self.in_color,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Zebra {
    #[serde(with = "serde_rgbu8")]
    pub even_color: Rgb<u8>,
    #[serde(with = "serde_rgbu8")]
    pub in_color: Rgb<u8>,
    #[serde(with = "serde_rgbu8")]
    pub odd_color: Rgb<u8>,
}

impl Default for Zebra {
    fn default() -> Self {
        Self {
            even_color: Rgb([0xff, 0xff, 0xff]),
            in_color: Rgb([0xaa, 0xaa, 0xaa]),
            odd_color: Rgb([0x00, 0x00, 0x00]),
        }
    }
}

impl Colorize<EscapeResult> for Zebra {
    fn colorize(&self, er: &EscapeResult) -> Rgb<u8> {
        match er.i {
            Some(i) if i % 2 == 0 => self.even_color,
            Some(_) => self.odd_color,
            None => self.in_color,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct ZebraMod {
    #[serde(with = "serde_rgbu8")]
    pub even_color: Rgb<u8>,
    #[serde(with = "serde_rgbu8")]
    pub in_color: Rgb<u8>,
    #[serde(with = "serde_rgbu8")]
    pub odd_color: Rgb<u8>,
    #[serde(with = "serde_rgbu8")]
    pub stripe_color: Rgb<u8>,
    pub stripe_freq: u64,
}

impl Default for ZebraMod {
    fn default() -> Self {
        Self {
            even_color: Rgb([0xff, 0xff, 0xff]),
            in_color: Rgb([0xaa, 0xaa, 0xaa]),
            odd_color: Rgb([0x00, 0x00, 0x00]),
            stripe_color: Rgb([0x00, 0xaa, 0xff]),
            stripe_freq: 10,
        }
    }
}

impl Colorize<EscapeResult> for ZebraMod {
    fn colorize(&self, er: &EscapeResult) -> Rgb<u8> {
        match er.i {
            Some(i) if i % self.stripe_freq == 0 => self.stripe_color,
            Some(i) if i % 2 == 0 => self.even_color,
            Some(_) => self.odd_color,
            None => self.in_color,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Auto {
    #[serde(with = "serde_rgbu8")]
    pub default_color: Rgb<u8>,
    pub phase: f64, // degrees
}

impl Default for Auto {
    fn default() -> Self {
        Self {
            default_color: Rgb([0, 0, 0]),
            phase: 7.5,
        }
    }
}

impl Colorize<SearchResult> for Auto {
    fn colorize(&self, sr: &SearchResult) -> Rgb<u8> {
        match sr.i {
            Some(i) => hsl_to_rgb(
                (i as f64 / sr.i_max as f64) * 360.0 + self.phase,
                0.95,
                0.05,
            ),
            None => self.default_color,
        }
    }
}

fn hsl_to_rgb(h: f64, s: f64, l: f64) -> Rgb<u8> {
    Rgb(Srgb::from_linear(Hsl::new(h, s, l).into_color()).into())
}
