use num_complex::Complex;
use serde::{Serialize, Deserialize};

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct ImageRect {
    pub width: u32,
    pub height: u32,
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct ComplexRect {
    pub min: Complex<f64>,
    pub max: Complex<f64>,
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct View {
    pub image: ImageRect,
    pub plane: ComplexRect,
}

impl Default for View {
    fn default() -> Self {
        // Scaling is off by 0.0016 of a pixel.
        Self::new(1280, 720, -2.6667, -1.5, 2.6667, 1.5)
    }
}

impl View {
    pub fn new(width: u32, height: u32, min_r: f64, min_i: f64, max_r: f64, max_i: f64) -> Self {
        Self {
            image: ImageRect { width, height },
            plane: ComplexRect {
                min: Complex::new(min_r, min_i),
                max: Complex::new(max_r, max_i),
            }
        }
    }

    pub fn dx(&self) -> f64 {
        (self.plane.max.re - self.plane.min.re) / self.image.width as f64
    }

    pub fn dy(&self) -> f64 {
        (self.plane.max.im - self.plane.min.im) / self.image.height as f64
    }

    pub fn pixel_to_plane(&self, x: u32, y: u32) -> Complex<f64> {
        Complex::new(
            self.plane.min.re + x as f64 * self.dx(),
            self.plane.max.im - y as f64 * self.dy(),
        )
    }

    pub fn center(&mut self, x: u32, y: u32) {
        let d = (self.plane.max - self.plane.min) / 2.0;
        let p = self.pixel_to_plane(x, y);

        self.plane.min = p - d;
        self.plane.max = p + d;
    }

    pub fn scale(&mut self, f: f64) {
        let c = self.plane.min + (self.plane.max - self.plane.min) / 2.0;

        self.plane.min = (self.plane.min - c) * f + c;
        self.plane.max = (self.plane.max - c) * f + c;
    }

}

mod test {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn test_pixel_to_plane() {
        let v = View::new(100, 100, -1.0, -1.0, 1.0, 1.0);
        let tests = vec![
            ((10, 10), Complex::new(-0.8, 0.8)),
            ((90, 90), Complex::new(0.8, -0.8)),
            ((10, 90), Complex::new(-0.8, -0.8)),
            ((90, 10), Complex::new(0.8, 0.8)),
        ];

        for ((x, y), expected) in tests {
            let z = v.pixel_to_plane(x, y);
            assert_eq!(expected, z);
        }
    }
}
